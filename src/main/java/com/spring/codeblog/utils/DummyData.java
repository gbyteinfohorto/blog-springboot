package com.spring.codeblog.utils;

import com.spring.codeblog.model.Post;
import com.spring.codeblog.repository.CodeblogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class DummyData {

    @Autowired
    CodeblogRepository codeblogRepository;

    //ANOTAÇÃO COMENTADA PARA NÃO GERAR MAIS POSTS AO INICIAR APLICAÇÃO
    //@PostConstruct
    public void savePosts(){

        //GERANDO O ARRAY LIST COM CADA POST DIRECIONADO AO IDICICE ESPECIFICO
        List<Post> postList = new ArrayList<>();

        Post post1 = new Post();
        post1.setAutor("1 Nome do Autor");
        post1.setData(LocalDate.now());
        post1.setTitulo("1 Titulo");
        post1.setTexto("1 AQUI ENCONTRA-SE O TEXTO / CONTEUDO DO POST");

        Post post2 = new Post();
        post2.setAutor("2 Nome do Autor");
        post2.setData(LocalDate.now());
        post2.setTitulo("2 Titulo");
        post2.setTexto("2AQUI ENCONTRA-SE O TEXTO / CONTEUDO DO POST");

        postList.add(post1);
        postList.add(post2);

        for(Post post: postList){
            Post postSaved = codeblogRepository.save(post);
            System.out.println("Postado Post c/ Id =>" + postSaved.getId());
        }
    }
}